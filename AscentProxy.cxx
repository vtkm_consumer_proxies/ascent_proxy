//============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//============================================================================

#include <VTKhProxyAPI.h>
#include <VTKhProxyRenderingAPI.h>

#include <vtkm/cont/ArrayHandle.h>
#include <vtkm/cont/ArrayHandleUniformPointCoordinates.h>
#include <vtkm/cont/CellSetStructured.h>
#include <vtkm/cont/CoordinateSystem.h>
#include <vtkm/cont/DataSet.h>
#include <vtkm/cont/Field.h>

vtkm::cont::DataSet construct_uniform_grid(const int dims[3]) {

  vtkm::cont::DataSet dataSet;
  vtkm::cont::ArrayHandleUniformPointCoordinates coords(
      {dims[0], dims[1], dims[2]}, {0, 0, 0}, {1.f, 1.f, 1.f});
  vtkm::cont::CoordinateSystem cs("points", coords);
  dataSet.AddCoordinateSystem(cs);

  vtkm::cont::CellSetStructured<3> cellSet;
  cellSet.SetPointDimensions(vtkm::Id3(dims[0], dims[1], dims[2]));
  dataSet.SetCellSet(cellSet);

  // add point scalar field
  vtkm::Float64 *pointvar = new vtkm::Float64[coords.GetNumberOfValues()];

  const vtkm::Float64 dx = (4.0 * vtkm::Pi()) / vtkm::Float64(dims[0] - 1);
  const vtkm::Float64 dy = (2.0 * vtkm::Pi()) / vtkm::Float64(dims[1] - 1);
  const vtkm::Float64 dz = (3.0 * vtkm::Pi()) / vtkm::Float64(dims[2] - 1);

  std::size_t idx = 0;
  for (vtkm::Id z = 0; z < dims[2]; ++z) {
    vtkm::Float64 cz = vtkm::Float64(z) * dz - 1.5 * vtkm::Pi();
    for (vtkm::Id y = 0; y < dims[1]; ++y) {
      vtkm::Float64 cy = vtkm::Float64(y) * dy - vtkm::Pi();
      for (vtkm::Id x = 0; x < dims[0]; ++x) {
        vtkm::Float64 cx = vtkm::Float64(x) * dx - 2.0 * vtkm::Pi();
        vtkm::Float64 cv =
            vtkm::Sin(cx) + vtkm::Sin(cy) +
            2.0 * vtkm::Cos(vtkm::Sqrt((cx * cx) / 2.0 + cy * cy) / 0.75) +
            4.0 * vtkm::Cos(cx * cy / 4.0);

        if (dims[2] > 1) {
          cv += vtkm::Sin(cz) +
                1.5 * vtkm::Cos(vtkm::Sqrt(cx * cx + cy * cy + cz * cz) / 0.75);
        }
        pointvar[idx++] = cv;
      }
    } // y
  }   // z

  vtkm::cont::ArrayHandle<vtkm::Float64> pointField =
      vtkm::cont::make_ArrayHandle(pointvar, coords.GetNumberOfValues());
  auto pfield = vtkm::cont::Field(
      "proxy_field", vtkm::cont::Field::Association::POINTS, pointField);
  dataSet.AddField(pfield);
  return dataSet;
}

bool simulate_using_ascent(const int dims[3],
                           const std::string &file_name) {
  const std::string field_name("proxy_field");
  auto input = construct_uniform_grid(dims);

  vtkm::Range r;
  input.GetField(field_name).GetRange(&r);
  std::vector<vtkm::Float64> iso_values{ r.Center() };

  auto to_clip = contour_with_isovalues(input, field_name, iso_values);

  auto to_clean = clip_with_sphere(to_clip,
                                   {30.f, 30.f, 30.f},
                                   50.f);

  auto to_render = clean_grid(to_clean);

  auto bounds = to_render.GetCoordinateSystem().GetBounds();

  vtkm::Vec<vtkm::Float64, 3> totalExtent{bounds.X.Length(), bounds.Y.Length(),
                                          bounds.Z.Length()};
  const auto mag = vtkm::Magnitude(totalExtent);
  vtkm::Normalize(totalExtent);

  vtkm::rendering::Camera camera;
  camera.SetLookAt(totalExtent * (mag * .5f));
  camera.SetViewUp(vtkm::make_Vec(0.f, 1.f, 0.f));
  camera.SetClippingRange(1.f, 100.f);
  camera.SetFieldOfView(60.f);
  camera.SetPosition(totalExtent * (mag * 2.f));

  return render_data(to_render, field_name, bounds, {512, 512}, camera,
                     file_name);
}
