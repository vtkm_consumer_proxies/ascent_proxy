
#include <AscentProxy.h>


int main(int, char**)
{
  int dims[3] = {64,64,64};
  std::string output_name = "ascent_demo.pnm";
  return !simulate_using_ascent(dims,output_name);
}
